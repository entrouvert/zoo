from pathlib import Path

import nox

nox.options.reuse_venv = True


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version='>=4.2,<4.3', drf_version='>=3.9,<3.15'):
    packages = [
        'WebTest',
        'astroid<3',
        'coverage',
        'cssselect',
        'django-webtest',
        f'django{django_version}',
        f'djangorestframework{drf_version}',
        'faker',
        'httmock',
        'lxml',
        'mock<4',
        'pip>8',
        'psycopg2-binary',
        'pylint-django',
        'pylint<3',
        'pyquery',
        'pytest-cov',
        'pytest-django',
        'pytest-flakes',
        'pytest-freezer',
        'pytest>=3.3.0',
        'pytz',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session
@nox.parametrize('django_version,drf_version', [('>=4.2,<4.3', '>=3.13,<3.15')], ids=['django4'])
def tests(session, django_version, drf_version):
    setup_venv(session, django_version=django_version, drf_version=drf_version)

    session.run('python', 'manage.py', 'compilemessages', silent=True)

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')

        args += ['--junitxml=junit-django.xml', '--cov=zoo/', '--cov-report', 'term-missing']

        if not session.interactive:
            args += [
                '-v',
                '--cov-report',
                'html',
                '--cov-report',
                'xml',
            ]

    args += session.posargs

    hookable_run(
        session,
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'tests.settings',
            'SETUPTOOLS_USE_DISTUTILS': 'stdlib',
        },
    )


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
