import csv
import datetime
import random

import django_webtest
import faker
import pytest
from django.contrib.auth.models import User
from django.core.management import call_command

from zoo.zoo_data.models import Entity, Relation, Transaction
from zoo.zoo_meta.models import EntitySchema, RelationSchema
from zoo.zoo_nanterre import utils
from zoo.zoo_nanterre.utils import age_in_years_and_months


@pytest.fixture
def rsu_schema(transactional_db):
    call_command('loaddata', 'rsu')
    individu = EntitySchema.objects.get(slug=utils.INDIVIDU_ENT)
    adresse = EntitySchema.objects.get(slug=utils.ADRESSE_ENT)
    relation_responsabilite_legale = RelationSchema.objects.get(slug=utils.RESPONSABILITE_LEGALE_REL)
    relation_habite = RelationSchema.objects.get(slug=utils.HABITE_REL)
    relation_union = RelationSchema.objects.get(slug=utils.UNION_REL)
    return {
        utils.INDIVIDU_ENT: individu,
        utils.ADRESSE_ENT: adresse,
        utils.RESPONSABILITE_LEGALE_REL: relation_responsabilite_legale,
        utils.HABITE_REL: relation_habite,
        utils.UNION_REL: relation_union,
    }


@pytest.fixture
def nanterre_classic_family(app, rsu_schema):
    entities = {
        'jean': {
            'slug': utils.INDIVIDU_ENT,
            'genre': 'homme',
            'prenoms': 'JEAN',
            'nom_de_naissance': 'DARMETTE',
            'nom_d_usage': '',
            'date_de_naissance': '1981-01-01',
            'telephones': [],
            'statut_legal': 'majeur',
            'email': 'jean.darmette@example.com',
            'cles_de_federation': {},
            'telephones': [
                {'type': 'pro', 'numero': '0101010908'},
                {'type': 'mobile', 'numero': '0606060606'},
            ],
        },
        'marie': {
            'slug': utils.INDIVIDU_ENT,
            'genre': 'femme',
            'prenoms': 'MARIE',
            'nom_de_naissance': 'NOËL',
            'nom_d_usage': 'DARMETTE',
            'date_de_naissance': '1982-01-01',
            'telephones': [],
            'statut_legal': 'majeur',
            'email': 'marie.darmette@example.com',
            'cles_de_federation': {},
            'telephones': [
                {'type': 'pro', 'numero': '0101010908'},
                {'type': 'mobile', 'numero': '0606060606'},
            ],
        },
        'kevin': {
            'slug': utils.INDIVIDU_ENT,
            'genre': 'homme',
            'prenoms': 'KÉVIN',
            'nom_de_naissance': 'DARMETTE',
            'nom_d_usage': '',
            'date_de_naissance': '2008-01-01',
            'telephones': [],
            'statut_legal': 'mineur',
            'email': '',
            'cles_de_federation': {},
        },
        'lilou': {
            'slug': utils.INDIVIDU_ENT,
            'genre': 'femme',
            'prenoms': 'LILOU',
            'nom_de_naissance': 'DARMETTE',
            'nom_d_usage': '',
            'date_de_naissance': '1982-01-01',
            'telephones': [],
            'statut_legal': 'mineur',
            'email': 'marie.darmette@example.com',
            'cles_de_federation': {},
        },
        'adresse': {
            'slug': utils.ADRESSE_ENT,
            'at': '',
            'streetnumber': '123',
            'streetnumberext': '',
            'streetname': 'ALLÉE DE L\'ARLEQUIN',
            'ext1': '',
            'ext2': '',
            'streetmatriculation': '00032',
            'zipcode': '92000',
            'inseecode': '92000',
            'city': 'NANTERRE',
            'country': 'FRANCE',
        },
    }
    relations = [
        {
            'slug': utils.HABITE_REL,
            'left': 'jean',
            'right': 'adresse',
            'content': {
                'principale': False,
            },
        },
        {
            'slug': utils.HABITE_REL,
            'left': 'marie',
            'right': 'adresse',
            'content': {
                'principale': False,
            },
        },
        {
            'slug': utils.HABITE_REL,
            'left': 'kevin',
            'right': 'adresse',
            'content': {
                'principale': False,
            },
        },
        {
            'slug': utils.HABITE_REL,
            'left': 'lilou',
            'right': 'adresse',
            'content': {
                'principale': False,
            },
        },
        {
            'slug': utils.UNION_REL,
            'left': 'jean',
            'right': 'marie',
            'content': {
                'statut': 'pacs/mariage',
            },
        },
        {
            'slug': utils.RESPONSABILITE_LEGALE_REL,
            'left': 'jean',
            'right': 'kevin',
            'content': {
                'statut': 'parent',
            },
        },
        {
            'slug': utils.RESPONSABILITE_LEGALE_REL,
            'left': 'marie',
            'right': 'kevin',
            'content': {
                'statut': 'parent',
            },
        },
        {
            'slug': utils.RESPONSABILITE_LEGALE_REL,
            'left': 'jean',
            'right': 'lilou',
            'content': {
                'statut': 'parent',
            },
        },
        {
            'slug': utils.RESPONSABILITE_LEGALE_REL,
            'left': 'marie',
            'right': 'lilou',
            'content': {
                'statut': 'parent',
            },
        },
    ]

    result = {}

    from django.db.transaction import atomic

    with atomic():
        tr = Transaction.get_transaction()
    for key, content in entities.items():
        content = content.copy()
        slug = content.pop('slug')
        result[key] = Entity.objects.create(created=tr, schema=rsu_schema[slug], content=content)

    for relation in relations:
        Relation.objects.create(
            created=tr,
            schema=rsu_schema[relation['slug']],
            left=result[relation['left']],
            right=result[relation['right']],
            content=relation['content'],
        )
    return result


@pytest.fixture
def rsu(rsu_schema):
    # populate individus
    fake = faker.Factory.create('fr_FR')

    sexes = ['femme', 'homme', 'autre']

    tr = Transaction.objects.create(meta='initial import')
    entities = []
    dates_seen = set()

    for i in range(1000):
        individu = {}
        individu['genre'] = genre = sexes[random.randint(0, 2)]
        individu['email'] = ('%05d' % i) + fake.email()
        if genre == 'femme':
            individu['prenoms'] = fake.first_name_female()
            individu['nom_de_naissance'] = fake.last_name_female()
            if random.randint(0, 10) == 0:
                individu['nom_d_usage'] = fake.last_name_male()
        else:
            individu['prenoms'] = fake.first_name_male()
            individu['nom_de_naissance'] = fake.last_name_male()
        while True:
            date_de_naissance = fake.date_time_between_dates(
                datetime_start=datetime.datetime(1930, 1, 1),
                datetime_end=datetime.datetime.now() + datetime.timedelta(days=120),
                tzinfo=None,
            ).date()
            if date_de_naissance not in dates_seen:
                dates_seen.add(date_de_naissance)
                break
        individu['date_de_naissance'] = date_de_naissance.isoformat()
        individu['statut_legal'] = (
            'majeur' if age_in_years_and_months(date_de_naissance) >= (18, 0) else 'mineur'
        )
        individu['cles_de_federation'] = {
            'technocarte': 'ABCD-%d' % i,
        }
        entities.append(Entity(created=tr, schema=rsu_schema[utils.INDIVIDU_ENT], content=individu))

    Entity.objects.bulk_create(entities)
    return entities


@pytest.fixture
def admin(db):
    user = User.objects.create(username='admin', is_staff=True, is_superuser=True)
    user.set_password('admin')
    user.save()
    return user


@pytest.fixture
def app(request, admin):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    app = django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': 'localhost'})
    app.authorization = ('Basic', ('admin', 'admin'))
    return app


@pytest.fixture
def app_noauth(request, admin):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    app = django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': 'localhost'})
    return app


@pytest.fixture
def lot_of_names(rsu_schema):
    try:
        reader = csv.reader(open('tests/export-etat-civil.csv'))
    except OSError:
        pytest.skip()

    def generate():
        tr = Transaction.objects.create()
        for genre, statut_legal, prenoms, nom_de_naissance, nom_d_usage, date_de_naissance in reader:
            yield Entity(
                schema=rsu_schema['individu'],
                created=tr,
                content={
                    'genre': genre,
                    'prenoms': prenoms,
                    'nom_de_naissance': nom_de_naissance,
                    'nom_d_usage': nom_d_usage,
                    'date_de_naissance': date_de_naissance,
                    'telephones': [],
                    'statut_legal': statut_legal,
                    'email': '',
                    'cles_de_federation': {},
                },
            )

    Entity.objects.bulk_create(generate())
