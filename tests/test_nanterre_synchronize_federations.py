import csv
import io

from webtest import Upload


def test_synchronize_federations(settings, app, nanterre_classic_family, admin):
    settings.NANTERRE_SYNCHRONIZE_FEDERATIONS_DO_LATER = False
    f = nanterre_classic_family

    # ajout de clés de fédération technocarte
    for entity in f.values():
        entity.content.setdefault('cles_de_federation', {})['technocarte'] = str(entity.id + 1000)
        entity.save()

    response = app.get('/admin/').follow()
    response.form.set('username', 'admin')
    response.form.set('password', 'admin')
    response = response.form.submit().follow()

    response = response.click('Synchroniser les fédérations')
    response = response.click('Nouvel import')
    form = response.forms['synchronize-federations-form']
    form.set('app_id', 'technocarte')
    content = ('\n'.join(map(str, [f['kevin'].id + 1000, f['marie'].id + 1000, '99999']))).encode()
    form.set('csv_uploaded', Upload('federations.csv', content, 'application/octet-stream'))
    response = form.submit().follow()
    assert len(response.pyquery('table#result-list tbody tr')) == 1
    response = response.click('Rapport')

    def check_csv_response(csv_response):
        reader = csv.DictReader(io.StringIO(csv_response.text))
        reader.fieldnames = reader.reader.__next__()
        rows = list(reader)

        def rows_by_action(action):
            return [row for row in rows if row['action'] == action]

        assert len(rows_by_action('KEEP')) == 2
        assert len(rows_by_action('DELETE')) == 2
        assert len(rows_by_action('UNKNOWN')) == 1
        assert len({row['action'] for row in rows}) == 3

    csv_response = response.click('CSV')
    check_csv_response(csv_response)

    response = response.click('Synchroniser les fédérations')

    response = response.forms['synchronize-federations-apply-form'].submit().follow()

    response = response.click('Rapport d\'exécution')

    csv_response = response.click('CSV')
    check_csv_response(csv_response)
