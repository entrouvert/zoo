import datetime
import json

import httmock
import pytest
from django.urls import reverse
from django.utils.timezone import now

from zoo.zoo_data.models import Job
from zoo.zoo_nanterre.qf import QF


@pytest.fixture
def qf_settings(settings):
    app = settings.ZOO_NANTERRE_APPLICATIONS.setdefault('implicit', {})
    app['rsu_ws_url'] = 'http://qf.example.org/'
    app['url'] = 'http://qf.example.org/'


def test_lire_quotients_valides(app, qf_settings, db):
    @httmock.urlmatch()
    def qf_ok(url, request):
        return json.dumps(
            [
                {
                    'nature-qf': 1,
                    'libelle': 'QF 2016',
                },
                {
                    'nature-qf': 2,
                    'libelle': 'QF 2017',
                },
                {
                    'nature-qf': 3,
                    'libelle': 'WTF',
                },
            ]
        )

    @httmock.urlmatch()
    def qf_nok(url, request):
        return {
            'status_code': 400,
            'content': json.dumps(
                {
                    'technique': 'vas te faire foutre',
                    'metier': 'veuillez recommencer',
                }
            ),
        }

    qf = QF()
    with httmock.HTTMock(qf_ok):
        response, error = qf.lire_quotients_valides(datetime.date.today())
    assert not error
    assert response[0]['annee_imposition'] == '2015'
    assert response[1]['annee_imposition'] == '2016'
    assert response[2]['annee_imposition'] == 'inconnue'

    with httmock.HTTMock(qf_nok):
        response, error = qf.lire_quotients_valides(datetime.date.today())
    assert not response
    assert error

    url = reverse('rsu-api-qf-lire-quotients-valides')
    with httmock.HTTMock(qf_ok):
        response = app.get(url)
    assert response.json['err'] == 0
    assert len(response.json['data']) == 3
    data = response.json['data']
    assert data[0]['annee_imposition'] == '2015'
    assert data[1]['annee_imposition'] == '2016'
    assert data[2]['annee_imposition'] == 'inconnue'

    with httmock.HTTMock(qf_nok):
        response = app.get(url)
    assert response.json['err'] == 1
    assert len(response.json['errors']) == 1


def test_simuler_qf(qf_settings, app, db):
    @httmock.urlmatch()
    def qf_ok(url, request):
        return json.dumps(
            [{'id': 1, 'montant': 12.43, 'tranche': 1, 'categorie': 'c1', 'id-qf': 14, 'nature-qf': 12345}]
        )

    qf = QF()
    with httmock.HTTMock(qf_ok):
        response, error = qf.simuler_qf(1234, 1234, 1345, 12345, False)
    assert not error

    url = reverse('rsu-api-qf-simuler')
    with httmock.HTTMock(qf_ok):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': 1,
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 0
    assert response.json['data']

    with httmock.HTTMock(qf_ok):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': '3.5',
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 0
    assert response.json['data']
    with httmock.HTTMock(qf_ok):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': 3.5,
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 0
    assert response.json['data']


def test_calculer_qf(nanterre_classic_family, qf_settings):
    e = nanterre_classic_family

    result = {'id': 2, 'montant': 12.43, 'tranche': 1, 'categorie': 'c1', 'id-qf': 14, 'nature-qf': 12345}

    @httmock.urlmatch()
    def qf_ok(url, request):
        return json.dumps(
            [
                {
                    'id': 1,
                    'id-metier': '1234',
                },
                result,
            ]
        )

    qf = QF()
    with httmock.HTTMock(qf_ok):
        response, error = qf.calcul_qf(e['jean'], 1234, 1234, 1345, 12345, False, None, None, None)
    assert not error
    assert response == result
    e['jean'].refresh_from_db()
    assert e['jean'].content['cles_de_federation']['implicit'] == '1234'


def test_api_qf_calculer(nanterre_classic_family, qf_settings, app):
    e = nanterre_classic_family

    result = {'id': 2, 'montant': 12.43, 'tranche': 1, 'categorie': 'c1', 'id-qf': 14, 'nature-qf': 12345}

    @httmock.urlmatch()
    def qf_ok(url, request):
        return json.dumps(
            [
                {
                    'id': 1,
                    'id-metier': '1234',
                },
                result,
            ]
        )

    @httmock.urlmatch()
    def qf_ok2(url, request):
        return json.dumps(
            [
                result,
            ]
        )

    @httmock.urlmatch()
    def qf_rsu_error(url, request):
        return httmock.response(
            500,
            {
                'technique': 'mouai',
                'metier': 'coin',
            },
            {
                'Content-Type': 'application/json',
            },
        )

    @httmock.urlmatch()
    def qf_bad_response(url, request):
        return httmock.response(500, 'wtf')

    url = reverse(
        'rsu-api-qf-calculer',
        kwargs={
            'identifier': e['jean'].id,
        },
    )
    with httmock.HTTMock(qf_ok):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': 1,
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 0
    assert response.json['data']
    assert 'nature-qf' in response.json['data']
    job = Job.objects.latest('modified')
    assert job.state == Job.STATE_SUCCESS
    action = job.action
    assert list(action.individus) == [e['jean']]
    assert len(action.response) == 2
    assert 'nature-qf' in action.response[1]
    assert 'id-metier' in action.response[0]

    e['jean'].refresh_from_db()
    assert e['jean'].content['cles_de_federation']['implicit'] == '1234'

    with httmock.HTTMock(qf_ok2):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': '2.5',
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 0
    assert response.json['data']
    assert 'nature-qf' in response.json['data']
    job = Job.objects.latest('modified')
    assert job.state == Job.STATE_SUCCESS
    action = job.action
    assert list(action.individus) == [e['jean']]
    assert len(action.response) == 1
    assert action.response[0] == result

    with httmock.HTTMock(qf_ok2):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': 2.5,
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 0
    assert response.json['data']
    assert 'nature-qf' in response.json['data']
    assert Job.objects.latest('modified').state == Job.STATE_SUCCESS

    with httmock.HTTMock(qf_rsu_error):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': 1,
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 1
    assert response.json['errors']
    job = Job.objects.latest('modified')
    assert job.state == Job.STATE_UNRECOVERABLE_ERROR
    action = job.action
    assert list(action.individus) == [e['jean']]
    assert isinstance(action.response, dict)
    assert 'metier' in action.response
    assert 'technique' in action.response
    assert action.error
    assert action.error['code'] == 'rsu-error'
    assert 'metier' in action.error['content']

    with httmock.HTTMock(qf_bad_response):
        response = app.post(
            url,
            params={
                'nature_qf': 1234,
                'rfr': 1234,
                'nb_parts': 1,
                'annee_imposition': 1234,
                'monoparentalite': True,
            },
        )
    assert response.json['err'] == 1
    assert response.json['errors']
    job = Job.objects.latest('modified')
    assert job.state == Job.STATE_UNRECOVERABLE_ERROR
    action = job.action
    assert list(action.individus) == [e['jean']]
    assert action.response == {}
    assert action.error
    assert action.error['code'] == 'response-is-not-json'
    assert action.error['status-code'] == 500
    assert action.error['content'] == 'wtf'

    # they should not be redone usually, but we try it anyway
    job.state = Job.STATE_ERROR
    job.save()

    with httmock.HTTMock(qf_ok2):
        Job.redo(timestamp=now() + datetime.timedelta(seconds=1))

    job = Job.objects.latest('modified')
    assert job.state == Job.STATE_SUCCESS
    action = job.action
    assert list(action.individus) == [e['jean']]
    assert len(action.response) == 1
    assert action.response[0] == result


def test_editer_carte(nanterre_classic_family, qf_settings):
    e = nanterre_classic_family
    e['jean'].content['cles_de_federation']['implicit'] = '1234'
    e['jean'].save()

    content = b'lacarte'
    carte_url = 'http://carte.com/'

    @httmock.urlmatch()
    def qf_ok(url, request):
        if url.netloc == 'carte.com':
            return content
        else:
            return json.dumps(
                {
                    'lien': carte_url,
                }
            )

    qf = QF()
    with httmock.HTTMock(qf_ok):
        response, error = qf.editer_carte(e['jean'], 1)
    assert not error
    assert response == content


def test_api_editer_carte(app, nanterre_classic_family, qf_settings):
    e = nanterre_classic_family
    e['jean'].content['cles_de_federation']['implicit'] = '1234'
    e['jean'].save()

    content = b'lacarte'
    carte_url = 'http://carte.com/'

    url = reverse(
        'rsu-api-qf-editer-carte',
        kwargs={
            'identifier': e['jean'].id,
            'id_qf': 1,
        },
    )

    @httmock.urlmatch()
    def qf_ok(url, request):
        if url.netloc == 'carte.com':
            return content
        else:
            return json.dumps(
                {
                    'lien': carte_url,
                }
            )

    with httmock.HTTMock(qf_ok):
        response = app.get(url)
        assert response.content == content
