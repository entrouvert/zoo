import csv
import json

import pytest

# Ces tests ne sont pas exécutables publiquement car ils nécessitent des
# données personnelles pour être efficaces:
# Pour générer ces données faire dans psql:
#
#   \copy (select e.content->>'genre',
#                 e.content->>'statut_legal',
#                 e.content->>'prenoms',
#                 e.content->>'nom_de_naissance',
#                 e.content->>'nom_d_usage',
#                 e.content->>'date_de_naissance'
#          from zoo_data_entity as e, zoo_meta_entityschema as s
#          where e.schema_id = s.id and s.slug = 'individu') to '/tmp/export-etat-civil.csv' with csv;
#
# et copier le fichier export-etat-civil.csv dans le répertoire tests/, ensuite
# écrire un fichier basic-searches.csv dans ce même répertoire avec le format
# suivant:
# * 1ère colonne: la requête, ex.: "benjamin dauvergne"
# * 2ème colonne: un entier positif, n, indiquant que la réponse doit se trouver dans les n premiers résultats,
# * 3ème colonne: un dictionnaire au format JSON indiquant les attributs pour
# repérer l'entrée que l'on recherche, ex.: (JSON adapté pour le format CSV)
# "{""nom"": "DAUVERGNE", ""prenoms"": ""BENJAMIN""}"
#
# En l'absence de ces fichiers les tests sont ignorés.


@pytest.fixture
def basic_searches():
    try:
        reader = csv.reader(open('tests/basic-searches.csv'))
    except OSError:
        pytest.skip()

    def generate():
        for query, window, checks in reader:
            window = int(window)
            checks = json.loads(checks)

            yield query, window, checks

    return list(generate())


def check(record, checks):
    for key, value in checks.items():
        if record.get(key) != value:
            return False
    return True


def test_basic(app, basic_searches, lot_of_names):
    for query, window, checks in basic_searches:
        response = app.get('/rsu/search/', params={'q': query, 'limit': 15})
        data = response.json['data'][:window]
        texts = [(row['score'], row['text']) for row in response.json['data']]
        # vérifie que la réponse que l'on cherche est bien dans les `window`
        # premières réponses.
        assert any(
            record for record in data if check(record, checks)
        ), 'query %r does not match criterious %r in first %d records' % (query, checks, window)
