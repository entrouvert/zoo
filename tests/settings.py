import os

from zoo.settings import *

# integrate with pg_virtualenv by getting pg configuration from environment variables
for key in ('PGPORT', 'PGHOST', 'PGUSER', 'PGPASSWORD'):
    if key in os.environ:
        DATABASES['default'][key[2:]] = os.environ[key]
