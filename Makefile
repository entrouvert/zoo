.PHONY: init

init:
	dropdb zoo || true
	createdb zoo
	. ~/.virtualenvs/zoo/bin/activate; ./manage.py migrate && ./manage.py loaddata fixtures/admin.json rsu && ./manage.py runserver

publish:
	rsync --delete -avz ./ bdauvergne@nanterre.dev:zoo/

import:
	dropdb --if-exists nanterre_rsu
	createdb nanterre_rsu
	psql nanterre_rsu <nrsu.sql
	psql nanterre_rsu <swarm_nanterre.sql
	make -C load

load:
	time ./manage.py rsu-load-dump --verbosity=2 --dry "dbname=nanterre_rsu" authentic_users.json
