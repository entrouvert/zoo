# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django
from django.contrib.postgres.fields import jsonb
from django.db.models import CharField, DateField, Transform
from django.db.models.functions import Lower

try:
    from django.db.models.fields.json import KeyTransform, KeyTransformTextLookupMixin
except ImportError:
    from django.contrib.postgres.fields.jsonb import KeyTransformTextLookupMixin
    from django.db.models.fields.json import KeyTransform


class Unaccent(Transform):
    lookup_name = 'unaccent'
    function = 'immutable_unaccent'
    output_field = CharField()


CharField.register_lookup(Unaccent)

CharField.register_lookup(Lower)


class Normalize(Transform):
    lookup_name = 'normalize'
    function = 'immutable_normalize'
    output_field = CharField()


CharField.register_lookup(Normalize)


class Date(Transform):
    lookup_name = 'timestamp'
    function = 'immutable_date'
    output_field = DateField()


CharField.register_lookup(Date)


class JSONUnaccent(KeyTransformTextLookupMixin, Unaccent):
    pass


class JSONTimestamp(KeyTransformTextLookupMixin, Date):
    pass


class JSONNormalize(KeyTransformTextLookupMixin, Normalize):
    pass


KeyTransform.register_lookup(JSONUnaccent)
KeyTransform.register_lookup(JSONTimestamp)
KeyTransform.register_lookup(JSONNormalize)
