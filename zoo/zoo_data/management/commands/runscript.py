# hobo - portal to configure and deploy applications
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import runpy
import sys

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('args', metavar='args', nargs='+', help='Fixture labels.')

    def handle(self, *args, **options):
        fullpath = os.path.dirname(os.path.abspath(args[0]))
        sys.path.insert(0, fullpath)
        module_name = os.path.splitext(os.path.basename(args[0]))[0]
        sys.argv = args
        runpy.run_module(module_name)
