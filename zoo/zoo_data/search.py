# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db.models import CharField, Func, Value


class Unaccent(Func):
    function = 'immutable_unaccent'
    arity = 1


class Normalize(Func):
    function = 'immutable_normalize'
    arity = 1


class JSONRef(Func):
    function = ''
    arg_joiner = '->'

    def __init__(self, *expressions, **extra):
        jsonb = expressions[0]
        refs = map(Value, expressions[1:])
        super().__init__(jsonb, *refs, **extra)


class JSONTextRef(Func):
    function = ''
    arg_joiner = '->>'
    arity = 2
    output_field = CharField()

    def __init__(self, *expressions, **extra):
        jsonb = expressions[0]
        if len(expressions) > 2:
            jsonb = JSONRef(jsonb, *expressions[1:-1])
        ref = Value(expressions[-1])
        super().__init__(jsonb, ref, **extra)


class TextCat(Func):
    function = ''
    arg_joiner = ' || '
