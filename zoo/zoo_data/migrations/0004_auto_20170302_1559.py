#
# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('zoo_data', '0003_auto_20170104_1857'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='log',
            options={'ordering': ('-timestamp', '-id'), 'verbose_name': 'log', 'verbose_name_plural': 'logs'},
        ),
        migrations.AlterField(
            model_name='log',
            name='transaction',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to='zoo_data.Transaction',
                verbose_name='transaction',
            ),
        ),
        migrations.AlterField(
            model_name='relation',
            name='left',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='left_relations',
                to='zoo_data.Entity',
                verbose_name='left',
            ),
        ),
        migrations.AlterField(
            model_name='relation',
            name='right',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='right_relations',
                to='zoo_data.Entity',
                verbose_name='right',
            ),
        ),
    ]
