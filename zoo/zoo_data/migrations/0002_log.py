#
# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import django.contrib.postgres.fields.jsonb
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('zoo_data', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'timestamp',
                    models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='timestamp'),
                ),
                (
                    'content',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        blank=True, null=True, verbose_name='content'
                    ),
                ),
                ('url', models.URLField(blank=True, null=True, verbose_name='url')),
                (
                    'entity',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='zoo_data.Entity',
                        verbose_name='entity',
                    ),
                ),
                (
                    'transaction',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='zoo_data.Transaction',
                        verbose_name='transaction',
                    ),
                ),
            ],
            options={
                'ordering': ('timestamp',),
                'verbose_name': 'log',
                'verbose_name_plural': 'logs',
            },
        ),
    ]
