#
# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import django.contrib.postgres.fields.jsonb
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('zoo_meta', '0002_auto_20161214_1545'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entity',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'meta',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        blank=True, null=True, verbose_name='meta'
                    ),
                ),
                (
                    'content',
                    django.contrib.postgres.fields.jsonb.JSONField(blank=True, verbose_name='content'),
                ),
            ],
            options={
                'ordering': ('created',),
                'verbose_name': 'entity',
                'verbose_name_plural': 'entities',
            },
        ),
        migrations.CreateModel(
            name='Relation',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'meta',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        blank=True, null=True, verbose_name='meta'
                    ),
                ),
                (
                    'content',
                    django.contrib.postgres.fields.jsonb.JSONField(blank=True, verbose_name='content'),
                ),
            ],
            options={
                'ordering': ('created',),
                'verbose_name': 'relation',
                'verbose_name_plural': 'relations',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                (
                    'meta',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        blank=True, null=True, verbose_name='meta'
                    ),
                ),
                (
                    'content',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        blank=True, null=True, verbose_name='content'
                    ),
                ),
                ('failed', models.BooleanField(default=False, verbose_name='failed')),
                (
                    'result',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        blank=True, null=True, verbose_name='content'
                    ),
                ),
            ],
            options={
                'ordering': ('id',),
                'verbose_name': 'transaction',
                'verbose_name_plural': 'transactions',
            },
        ),
        migrations.AddField(
            model_name='relation',
            name='created',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='created_relations',
                to='zoo_data.Transaction',
                verbose_name='created',
            ),
        ),
        migrations.AddField(
            model_name='relation',
            name='deleted',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='deleted_relations',
                to='zoo_data.Transaction',
                verbose_name='deleted',
            ),
        ),
        migrations.AddField(
            model_name='relation',
            name='left',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='+',
                to='zoo_data.Entity',
                verbose_name='left',
            ),
        ),
        migrations.AddField(
            model_name='relation',
            name='modified',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='modified_relations',
                to='zoo_data.Transaction',
                verbose_name='modified',
            ),
        ),
        migrations.AddField(
            model_name='relation',
            name='right',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='+',
                to='zoo_data.Entity',
                verbose_name='right',
            ),
        ),
        migrations.AddField(
            model_name='relation',
            name='schema',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='zoo_meta.RelationSchema',
                verbose_name='schema',
            ),
        ),
        migrations.AddField(
            model_name='entity',
            name='created',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='created_entities',
                to='zoo_data.Transaction',
                verbose_name='created',
            ),
        ),
        migrations.AddField(
            model_name='entity',
            name='deleted',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='deleted_entities',
                to='zoo_data.Transaction',
                verbose_name='deleted',
            ),
        ),
        migrations.AddField(
            model_name='entity',
            name='modified',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='modified_entities',
                to='zoo_data.Transaction',
                verbose_name='modified',
            ),
        ),
        migrations.AddField(
            model_name='entity',
            name='schema',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to='zoo_meta.EntitySchema', verbose_name='schema'
            ),
        ),
    ]
