#
# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.contrib.postgres.operations import TrigramExtension, UnaccentExtension
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('zoo_meta', '0001_initial'),
    ]

    operations = [
        TrigramExtension(),
        UnaccentExtension(),
        migrations.RunSQL(
            [
                'CREATE OR REPLACE FUNCTION immutable_unaccent(text) RETURNS varchar AS $$ '
                " SELECT unaccent('unaccent',$1::text); $$ LANGUAGE 'sql' IMMUTABLE",
                'CREATE OR REPLACE FUNCTION immutable_timestamp(text) RETURNS timestamp AS $$ '
                'SELECT $1::timestamp; $$ LANGUAGE sql IMMUTABLE',
                'CREATE OR REPLACE FUNCTION immutable_normalize(text) RETURNS varchar AS $$ '
                " SELECT regexp_replace(lower(unaccent('unaccent', $1::text)), ' *([''-] *)+', 'x', 'g'); "
                '$$ LANGUAGE sql IMMUTABLE',
                'CREATE OR REPLACE FUNCTION immutable_date(text) RETURNS date AS $$ '
                'SELECT ($1)::date; $$ LANGUAGE sql IMMUTABLE',
            ],
            [],
        ),
    ]
