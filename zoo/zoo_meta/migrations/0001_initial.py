#
# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import django.contrib.postgres.fields.jsonb
import django.db.models.deletion
from django.db import migrations, models

import zoo.zoo_meta.validators


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='EntitySchema',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='name')),
                ('slug', models.SlugField(max_length=64, unique=True, verbose_name='slug')),
                (
                    'schema',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        validators=[zoo.zoo_meta.validators.validate_schema], verbose_name='schema'
                    ),
                ),
                ('caption_template', models.TextField(blank=True, verbose_name='caption template')),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'entity schema',
                'verbose_name_plural': 'entity schemas',
            },
        ),
        migrations.CreateModel(
            name='RelationSchema',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='name')),
                ('slug', models.SlugField(max_length=64, unique=True, verbose_name='slug')),
                (
                    'schema',
                    django.contrib.postgres.fields.jsonb.JSONField(
                        validators=[zoo.zoo_meta.validators.validate_schema], verbose_name='schema'
                    ),
                ),
                ('caption_template', models.TextField(blank=True, verbose_name='caption template')),
                ('is_symmetric', models.BooleanField(default=False, verbose_name='is symmetric')),
                (
                    'left',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='+',
                        to='zoo_meta.EntitySchema',
                        verbose_name='left schema',
                    ),
                ),
                (
                    'right',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='+',
                        to='zoo_meta.EntitySchema',
                        verbose_name='right schema',
                    ),
                ),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'relation schema',
                'verbose_name_plural': 'relation schemas',
            },
        ),
    ]
