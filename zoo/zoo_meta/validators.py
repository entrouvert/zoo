# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ValidationError
from jsonschema import Draft4Validator


def schema_validator(schema):
    def validate(value):
        errors = []
        validator = Draft4Validator(schema)
        for error in sorted(validator.iter_errors(value), key=str):
            errors.append(ValidationError(error.message))
        if errors:
            raise ValidationError(errors)

    return validate


validate_schema = schema_validator(Draft4Validator.META_SCHEMA)
validate_schema.__name__ = 'validate_schema'
