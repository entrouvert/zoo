# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.apps import AppConfig
from django.db.models.signals import post_migrate, post_save
from django.utils.translation import gettext_lazy as _


class ZooMetaAppConfig(AppConfig):
    name = 'zoo.zoo_meta'
    verbose_name = _('metadatas')

    def post_save(self, sender, instance, **kwargs):
        instance.rebuild_indexes()

    def ready(self):
        from .models import EntitySchema

        post_save.connect(self.post_save, sender=EntitySchema)
