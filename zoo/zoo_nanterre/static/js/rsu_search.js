$(function () {
        var xhr = null;
        var timeout = null;
        var last = null;

        function submit() {
                var $form = $('form');
                if (xhr) {
                        xhr.abort();
                }
                var query = $form.serialize();
                $('#id_spinner').addClass('spinner');
                xhr = $.ajax({
                        url: '.',
                        type: 'GET',
                        data: $form.serialize(),
                        success: function (html) {
                                last = query;
                                $('#id_spinner').removeClass('spinner');
				                var $result = $.parseHTML(html, document, true);
				                $('#id_results').replaceWith($('#id_results', $result));
                        },
                });
        }
        function reload() {
                if (last && last == $('form').serialize()) {
                        return;
                }
                $('#id_results').addClass('disabled-results');
                if (timeout != null) {
                        clearTimeout(timeout);
                        timeout = null;
                }
                if (xhr) {
                        xhr.abort();
                        $('#id_spinner').removeClass('spinner');
                        
                }
                timeout = setTimeout(function () {
                        submit();
                }, 300);
        }
        $(document).on('change keyup', 'form input', function (ev) {
                reload();
        })
})
