# /opt/zoo/local_settings.py

DEBUG = True
ADMINS = [
    ('Admins', 'root+zoo@example.net'),
]

EMAIL_SUBJECT_PREFIX = '[zoo] '
SERVER_EMAIL = 'root+zoo@example.net'

STATIC_ROOT = '/opt/zoo/collectstatic/'

DATABASES['default']['NAME'] = 'zoo'
DATABASES['default']['USER'] = 'zoo'
DATABASES['default']['PASSWORD'] = 'zoopass'
DATABASES['default']['HOST'] = 'sql'
DATABASES['default']['PORT'] = '5432'

# RSU
ZOO_NANTERRE_APPLICATIONS['technocarte']['rsu_ws_url'] = 'http://.../kiosque/rsu/libredemat/compte'
ZOO_NANTERRE_APPLICATIONS['infor']['rsu_ws_url'] = 'http://.../rsu.csp'
ZOO_NANTERRE_APPLICATIONS['implicit'][
    'rsu_ws_url'
] = 'http://.../SoapRestBridge_ws/rest/executeTraitement/cSchema=...'

# INVOICES / PAYMENT
ZOO_NANTERRE_APPLICATIONS['saga']['url'] = 'http://.../etat_facture_creance_literal'

# QF
ZOO_NANTERRE_APPLICATIONS['qf'] = {
    'name': 'QF',
    'url': 'http://.../SoapRestBridge_ws/rest/executeTraitement/cSchema=...',
}
ZOO_NANTERRE_APPLICATIONS['implicit'][
    'url'
] = 'http://.../SoapRestBridge_ws/rest/executeTraitement/cSchema=...'
