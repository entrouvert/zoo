# zoo - versatile objects management
#
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand

from zoo.zoo_nanterre.utils import LoadDump


class Command(BaseCommand):
    def handle(self, pg_dsn, authentic_fixture_path, **options):
        LoadDump(pg_dsn, authentic_fixture_path, verbosity=options['verbosity'], dry=options['dry']).load()

    def add_arguments(self, parser):
        parser.add_argument(
            'pg_dsn', help='DSN of the database containing RSU dump, ex.: ' 'dbname=nanterre_rsu'
        )
        parser.add_argument(
            'authentic_fixture_path', help='path for the authentic fixture containing existing SWARM users'
        )
        parser.add_argument('--dry', action='store_true', default=False, help='simulate')
