# zoo - versatile objects management
#
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.timezone import now

from zoo.zoo_data.models import Log
from zoo.zoo_nanterre.utils import passage_a_la_majorite


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.options = options
        self.clean_journal()
        self.passage_a_la_majorite()

    def clean_journal(self):
        '''Supprime les entrées du journal plus vieilles que ZOO_NANTERRE_LOG_EXPIRATION_DAYS'''
        duration = datetime.timedelta(days=settings.ZOO_NANTERRE_LOG_EXPIRATION_DAYS)
        n = now()
        n = n.replace(hour=0, minute=0, second=0, microsecond=0)
        threshold = n - duration
        count, count_by_class = Log.objects.filter(timestamp__lt=threshold).delete()
        if count and self.options['verbosity'] > 0:
            print('Suppression de %d entrées du journal.' % count)

    def passage_a_la_majorite(self):
        if not getattr(settings, 'ZOO_NANTERRE_PASSAGE_A_LA_MAJORITE', False):
            return

        result = passage_a_la_majorite()
        if result['errors'] and self.options['verbosity'] > 0:
            print('Mise à jour de %d enfants.' % result['updated_entities'])
            for individu in result['errors']:
                for error in result['errors'][individu]:
                    print('Erreur:', 'individu %s' % individu.pk, error)
        elif result['updated_entities'] and self.options['verbosity'] > 1:
            print('Mise à jour de %d enfants.' % result['updated_entities'])
