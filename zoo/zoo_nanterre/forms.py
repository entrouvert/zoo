# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from . import synchronize_federations
from .utils import PersonSearch, get_application, get_applications


class SearchForm(forms.Form):
    limit = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                'type': 'range',
                'min': '0',
                'max': '1',
                'step': '0.02',
            }
        ),
        required=False,
    )
    query = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'autocomplete': 'off',
            }
        )
    )

    def results(self):
        query = self.cleaned_data['query']
        try:
            limit = float(self.cleaned_data.get('limit'))
        except ValueError:
            limit = 0.5
        return iter(PersonSearch(limit=limit).search_query(query))


def application_choices():
    yield ('', '---')
    for slug in get_applications(rsu_ws_url=True):
        dfn = get_application(slug)
        yield (slug, dfn.get('name', slug))


class SynchronizeFederationsForm(forms.Form):
    app_id = forms.ChoiceField(choices=application_choices, label=_('Application'))
    csv_uploaded = forms.FileField(label=_('CSV file'))

    def clean_csv_uploaded(self):
        csv_uploaded = self.cleaned_data['csv_uploaded']
        errors = []
        csv_uploaded.seek(0)
        for i, line in enumerate(csv_uploaded):
            try:
                line.decode('ascii')
            except UnicodeError as e:
                errors.append(_('non-ASCII character on line {0} and column {1}').format(i + 1, e.start + 1))
        # restore file state
        csv_uploaded.seek(0)
        if errors:
            raise ValidationError(errors)
        return csv_uploaded

    def save(self):
        synchronize_federations.SynchronizeFederationsAction.synchronize(**self.cleaned_data)
