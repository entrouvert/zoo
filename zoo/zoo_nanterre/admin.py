# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin

from .models import Duplicate
from .utils import get_application, get_applications, individu_caption


@admin.register(Duplicate)
class DuplicateAdmin(admin.ModelAdmin):
    list_filter = ['state']
    list_display = ['created', 'modified', 'score', 'first_caption', 'second_caption', 'state']
    raw_id_fields = ['first', 'second']

    def first_caption(self, instance):
        return individu_caption(instance.first)

    def second_caption(self, instance):
        return individu_caption(instance.second)


class JobApplicationListFilter(admin.SimpleListFilter):
    title = 'application'

    parameter_name = 'application'

    def lookups(self, request, model_admin):
        l = []
        for app in get_applications(rsu_ws_url=True):
            app_dfn = get_application(app)
            l.append((app, app_dfn['name']))
        return l

    def queryset(self, request, queryset):
        if self.value():
            queryset = queryset.filter(content__application_id=self.value())
        return queryset
