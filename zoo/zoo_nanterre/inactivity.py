# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import operator

try:
    from functools import reduce
except ImportError:
    pass

from cached_property import cached_property
from django.db.models.query import Q
from django.utils.timezone import now

from zoo.zoo_data.models import Entity

from . import utils


class Inactivity:
    def __init__(self, child_delay=365, adult_delay=365):
        self.child_delay = child_delay
        self.adult_delay = adult_delay

    @property
    def child_threshold(self):
        return now() - datetime.timedelta(days=self.child_delay)

    @property
    def adult_threshold(self):
        return now() - datetime.timedelta(days=self.adult_delay)

    def exclude_newer_than_threshold(self, qs, threshold):
        return qs.exclude(
            Q(created__created__gte=threshold)
            | Q(modified__created__gte=threshold)
            | Q(log__timestamp__gte=threshold)
        )

    @property
    def query_no_federation(self):
        queries = []

        for app_id in utils.get_applications(rsu_ws_url=True):
            query = Q(**{'content__cles_de_federation__%s__isnull' % app_id: True})
            query |= Q(**{'content__cles_de_federation__%s__exact' % app_id: ''})
            queries.append(query)
        return reduce(operator.__and__, queries)

    def filter_no_federation(self, qs):
        return qs.filter(self.query_no_federation)

    @property
    def entities(self):
        qs = Entity.objects.all()
        # prefetch to optimize accesors to spouses and siblings
        qs = qs.prefetch_related(
            'left_relations__schema',
            'left_relations__right',
            'right_relations__schema',
            'right_relations__left',
            'right_relations__left__left_relations__left',
            'right_relations__left__left_relations__schema',
        )
        return qs

    @property
    def children(self):
        return self.entities.filter(content__statut_legal='mineur')

    @property
    def adults(self):
        return self.entities.filter(content__statut_legal='majeur')

    @cached_property
    def deletable_children(self):
        return self.exclude_newer_than_threshold(
            self.filter_no_federation(self.children), self.child_threshold
        )

    @cached_property
    def deletable_adults(self):
        deletable_children_ids = self.deletable_children.values_list('id', flat=True)
        potent = self.exclude_newer_than_threshold(
            self.filter_no_federation(self.adults), self.adult_threshold
        )

        def filter_children_are_deletable():
            for adult in potent:
                for enfant, rele in utils.enfants(adult):
                    if enfant.id not in deletable_children_ids:
                        break
                else:
                    yield adult

        potent2_ids = [adult.id for adult in filter_children_are_deletable()]
        return potent.filter(id__in=potent2_ids)
