# zoo - versatile objects management
# Copyright (w+) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your optiow+) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from . import api_views

urlpatterns = [
    path('search/', api_views.search, name='rsu-api-search'),
    re_path(r'^individu/(?P<identifier>[-\w]+)/$', api_views.reseau, name='rsu-api-reseau'),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/suppression/$',
        api_views.suppression_individu,
        name='rsu-api-suppression-individu',
    ),
    re_path(r'^individu/(?P<identifier>[-\w]+)/liste/$', api_views.reseau_liste, name='rsu-api-reseau-liste'),
    re_path(r'^individu/(?P<identifier>[-\w]+)/journal/$', api_views.journal, name='rsu-api-journal'),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/declaration-responsabilite-legale/$',
        api_views.declaration_responsabilite_legale,
        name='rsu-api-declaration-responsabilite-legale',
    ),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/declaration-adresse-principale/$',
        api_views.declaration_adresse_principale,
        name='rsu-api-declaration-adresse-principale',
    ),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/changement-de-situation-maritale/$',
        api_views.changement_de_situation_maritale,
        name='rsu-api-changement-de-situation-maritale',
    ),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/separation/$', api_views.separation, name='rsu-api-separation'
    ),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/declaration-de-deces/$',
        api_views.declaration_de_deces,
        name='rsu-api-declaration-de-deces',
    ),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/(?P<identifier_enfant>[-\w]+)/'
        'suppression-lien-de-responsabilite/$',
        api_views.suppression_lien_de_responsabilite,
        name='rsu-api-suppression-lien-de-responsabilite',
    ),
    path('individu/', api_views.create_individu, name='rsu-api-create-individu'),
    re_path(
        r'^individu/(?P<identifier>[-\w]+)/federation/(?P<application>\w+)/$',
        api_views.federation,
        name='rsu-api-federation',
    ),
    path('declaration-union/', api_views.declaration_union, name='rsu-api-declaration-union'),
    path('synchronisation/', api_views.synchronization, name='rsu-api-synchronization'),
    path('saga/retour-asynchrone/', api_views.saga_retour_asynchrone, name='rsu-api-saga-retour-asynchrone'),
    path('saga/retour-synchrone/', api_views.saga_retour_synchrone, name='rsu-api-saga-retour-synchrone'),
    re_path(
        r'^saga/tiers/(?P<application>\w+)/(?P<identifier>[-\w]+)/$',
        api_views.saga_tiers,
        name='rsu-api-saga-tiers',
    ),
    re_path(
        r'^saga/(?P<identifier>[-\w]+)/factures/$', api_views.saga_factures, name='rsu-api-saga-factures'
    ),
    re_path(
        r'^saga/(?P<identifier>[-\w]+)/transaction/$',
        api_views.saga_transaction,
        name='rsu-api-saga-transaction',
    ),
    path(
        'qf/lire-quotients-valides/',
        api_views.qf_lire_quotiens_valides,
        name='rsu-api-qf-lire-quotients-valides',
    ),
    path('qf/simuler/', api_views.qf_simuler, name='rsu-api-qf-simuler'),
    re_path(r'^qf/(?P<identifier>[-\w]+)/$', api_views.qf_calculer, name='rsu-api-qf-calculer'),
    re_path(
        r'^qf/(?P<identifier>[-\w]+)/editer-carte/(?P<id_qf>\w+)/$',
        api_views.qf_editer_carte,
        name='rsu-api-qf-editer-carte',
    ),
    path('doublons/', api_views.doublons, name='rsu-api-doublons'),
    re_path(r'^doublons/(?P<doublon_id>[0-9 ]+)/$', api_views.doublon, name='rsu-api-doublon'),
    re_path(
        r'^doublons/(?P<doublon_id>[0-9 ]+)/false-positive/$',
        api_views.false_positive,
        name='rsu-api-doublon-false-positive',
    ),
    re_path(r'^doublons/(?P<doublon_id>[0-9 ]+)/dedup/$', api_views.dedup, name='rsu-api-doublon-dedup'),
]
