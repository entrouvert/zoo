#
# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import functools

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.urls import re_path
from django.utils.translation import gettext_lazy as _


class ZooNanterreConfig(AppConfig):
    name = 'zoo.zoo_nanterre'
    verbose_name = _('Nanterre')

    def get_entity_actions(self):
        from . import fragments, utils

        def synchronize(application, modeladmin, request, queryset):
            qs = queryset.filter(schema__slug=utils.INDIVIDU_ENT)
            list(fragments.synchronize([application], qs))

        actions = {}
        for application in utils.get_applications(rsu_ws_url=True):
            app_dfn = utils.get_application(application)
            name = 'Synchroniser %s' % app_dfn['name']
            actions[name] = (functools.partial(synchronize, application), name, name)
        return actions

    def get_job_list_filter(self):
        from .admin import JobApplicationListFilter

        return [JobApplicationListFilter]

    def get_entityadmin_urls(self, model_admin):
        from . import views

        descs = [
            {
                're': r'',
                'view': '',
                'name': '',
            },
            {
                're': r'add/',
                'view': '_add',
                'name': '-new-import',
            },
            {
                're': r'(?P<job_id>\d+)/download-report/(?P<filename>.*)',
                'view': '_download_report',
                'name': '-download-report',
            },
            {
                're': r'(?P<job_id>\d+)/report/(?P<filename>.*)',
                'view': '_report',
                'name': '-show-report',
            },
            {
                're': r'(?P<job_id>\d+)/download-apply-report/(?P<filename>.*)',
                'view': '_download_apply_report',
                'name': '-download-apply-report',
            },
            {
                're': r'(?P<job_id>\d+)/apply-report/(?P<filename>.*)',
                'view': '_apply_report',
                'name': '-show-apply-report',
            },
            {
                're': r'(?P<job_id>\d+)/delete/',
                'view': '_delete',
                'name': '-delete',
            },
            {
                're': r'(?P<job_id>\d+)/apply/',
                'view': '_apply',
                'name': '-apply',
            },
        ]

        urls = []
        for desc in descs:
            urls.append(
                re_path(
                    r'^synchronize-federations/%s$' % desc['re'],
                    model_admin.admin_site.admin_view(
                        getattr(views, 'synchronize_federations' + desc['view'])
                    ),
                    kwargs={'model_admin': model_admin},
                    name='synchronize-federations' + desc['name'],
                )
            )
        urls.append(
            re_path(
                r'^inactive/',
                model_admin.admin_site.admin_view(getattr(views, 'inactive_index')),
                kwargs={'model_admin': model_admin},
                name='inactive-index',
            )
        )
        return urls

    def post_migrate(self, *args, **kwargs):
        from django.contrib.auth.models import Permission

        Permission.objects.filter(codename='action1_entity').update(name='Peut synchroniser les fédérations')

    def ready(self):
        post_migrate.connect(self.post_migrate)

    def zoo_rebuild_indexes(self, schema):
        from .utils import INDIVIDU_ENT

        if schema.slug != INDIVIDU_ENT:
            return

        schema.create_trigram_index(
            "immutable_normalize(content->>'prenoms' || ' ' || (content->>'nom_de_naissance'))"
        )
        schema.create_trigram_index(
            "immutable_normalize(content->>'prenoms' || ' ' || (content->>'nom_d_usage'))"
        )
        schema.create_trigram_index("immutable_normalize(content->>'prenoms')")
        schema.create_trigram_index("immutable_normalize(content->>'nom_d_usage')")
        schema.create_trigram_index("immutable_normalize(content->>'nom_de_naissance')")
