# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import CharField, DecimalField, Form, NumberInput, TextInput

from zoo.models import Entity


class EntitySearchForm(Form):
    def __init__(self, *args, **kwargs):
        self.schema = kwargs.pop('schema')
        super().__init__(*args, **kwargs)
        self.fields['limit'] = DecimalField(
            widget=NumberInput(
                attrs={
                    'type': 'range',
                    'min': '0',
                    'max': '1',
                    'step': '0.02',
                }
            ),
            required=False,
        )
        for path, _type, _format in self.schema.paths():
            if _type != 'string':
                continue
            key = '__'.join(path)
            self.fields[key] = CharField(
                max_length=32,
                required=False,
                widget=TextInput(attrs={'autocomplete': 'off'}),
            )

    def search(self):
        kwargs = {}
        for key in self.fields:
            if key == 'limit':
                continue
            if self.cleaned_data.get(key):
                kwargs[key] = self.cleaned_data[key]
        limit = self.cleaned_data.get('limit') or None
        if kwargs:
            return Entity.objects.content_search(self.schema, limit=limit, **kwargs)
        else:
            return Entity.objects.none()
