# zoo - versatile objects management
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import get_object_or_404, render

from zoo.models import Entity, EntitySchema

from .forms import EntitySearchForm


def schemas(request):
    '''Show all schemas and give link to search page'''
    return render(
        request,
        'zoo_demo/schemas.html',
        {
            'schemas': EntitySchema.objects.all(),
        },
    )


def schema(request, name):
    '''Display a search form for the targeted schema'''
    schema = get_object_or_404(EntitySchema, name=name)
    qs = Entity.objects.filter(schema=schema)
    form = EntitySearchForm(schema=schema, data=request.GET)
    if form.is_valid():
        qs = form.search()
    else:
        qs = Entity.objects.none()
    try:
        page = int(request.GET.get('page', 0))
    except ValueError:
        page = 0
    qs = qs[page * 10 : (page + 1) * 10]
    next_page = page + 1
    if len(qs) < 10:
        next_page = None
    return render(
        request,
        'zoo_demo/schema.html',
        {
            'schema': schema,
            'form': form,
            'entities': qs,
            'next_page': next_page,
            'previous_page': page - 1,
        },
    )
