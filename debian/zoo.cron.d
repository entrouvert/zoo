MAILTO=root

# "tenant_command --all-tenants" commands are disabled if DISABLE_CRON_JOBS is True

0,10,20,30,40,50 * * * * zoo test -x /usr/bin/zoo-manage && /usr/bin/zoo-manage tenant_command runjobs -v0 --all-tenants

0 0 * * * zoo test -x /usr/bin/zoo-manage && /usr/bin/zoo-manage tenant_command rsu-cron -v0 --all-tenants
0 0 * * * zoo test -x /usr/bin/zoo-manage && /usr/bin/zoo-manage tenant_command rsu-duplicates -v0 --all-tenants find --limit 0.55
