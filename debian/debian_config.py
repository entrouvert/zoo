# This file is sourced by "execfile" from zoo.settings

import os

from django.conf import global_settings

PROJECT_NAME = 'zoo'

#
# hobotization (multitenant)
#
exec(open('/usr/lib/hobo/debian_config_common.py').read())

# re-enable atomic requests (erased by hobo/debian_config_common.py)
DATABASES['default']['ATOMIC_REQUESTS'] = True

# Cancel mellon specific settings

# DRF authentication does not use mellon (NameID= in publik signature)
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
}
# no mellon/SAML sessionNotOnOrAfter session expiration
SESSION_ENGINE = global_settings.SESSION_ENGINE
# remove mellon middleware
MIDDLEWARE = tuple(
    mw
    for mw in MIDDLEWARE
    if not mw.startswith('mellon.') and not mw.startswith('hobo.provisionning.middleware.')
)
if 'MIDDLEWARE_CLASSES' in globals():
    MIDDLEWARE_CLASSES = tuple(
        m
        for m in MIDDLEWARE_CLASSES
        if not m.startswith('mellon.') and not mw.startswith('hobo.provisionning.middleware.')
    )
# remove mellon settings_loader
TENANT_SETTINGS_LOADERS = tuple(tsl for tsl in TENANT_SETTINGS_LOADERS if not tsl.endswith('.Mellon'))


#
# local settings
#
exec(open(os.path.join(ETC_DIR, 'settings.py')).read())

# run additional settings snippets
exec(open('/usr/lib/hobo/debian_config_settings_d.py').read())
